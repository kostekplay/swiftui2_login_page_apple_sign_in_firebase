////  SwiftUI2_LoginPageAppleSignInFirebaseApp.swift
//  SwiftUI2_LoginPageAppleSignInFirebase
//
//  Created on 22/02/2021.
//  
//

import SwiftUI

@main
struct SwiftUI2_LoginPageAppleSignInFirebaseApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
